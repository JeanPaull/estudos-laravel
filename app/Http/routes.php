<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     //return view('welcome');
//     return view('template_teste.index');
// });

Route::get('contato', function(){
	return "Return Página de Contato";
});

Route::match(['get', 'post'], 'pagina', function(){
	return "Passando os dois métodos get e post";
});
//Obs: Para aceitar qualquer método Route::any(...


Route::auth();
Route::get('/home', 'HomeController@home');
Route::get('/', 'HomeController@index');
